import { Data } from "./Data.js";

export class Modal {
  constructor(formContent) {
    this.formContent = formContent;
    this.data = new Data(this);
  }

  render() {
    const modalWindow = document.createElement("div");
    const modalContent = document.createElement("div");
    const modalCloseButton = document.createElement("div");

    modalWindow.classList.add("modal");
    modalContent.classList.add("modal__content");
    modalCloseButton.classList.add("modal__close-button");
    modalCloseButton.onclick = () => this.close();
    modalWindow.onclick = () => this.close();
    modalContent.onclick = (e) => e.stopPropagation();
    modalCloseButton.innerHTML = "&times;";

    document.body.append(modalWindow);
    modalWindow.append(modalContent);
    modalContent.append(modalCloseButton, this.formContent);
  }

  close() {
    const modalWindow = document.querySelector(".modal");
    modalWindow.remove();
  }
}
