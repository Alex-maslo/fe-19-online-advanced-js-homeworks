"use strict";
import { Form } from "./Form.js";

export class LoginForm extends Form {
  constructor(formId, formTitle) {
    super(formId, formTitle);
  }

  render() {
    const formElement = super.render();

    const loginInput = this.createInput(
      "text",
      "login",
      "input-form",
      "login-input"
    );
    const passwordInput = this.createInput(
      "password",
      "password",
      "input-form",
      "password-input"
    );

    const submitButton = this.createButton(
      "submit",
      "Вхід",
      "login-form__submit-button"
    );

    const messageParagraph = this.createParagraph("Невірний логін або пароль");

    formElement.append(
      loginInput,
      passwordInput,
      submitButton,
      messageParagraph
    );

    return formElement;
  }
}
