"use strict";

export function createComponent(object) {
  const { tagName, attributes = {}, textContent = "" } = object;
  const element = document.createElement(tagName);

  for (const attribute in attributes) {
    if (attributes[attribute] !== undefined) {
      element.setAttribute(attribute, attributes[attribute]);
    }
  }

  if (textContent !== "") {
    element.textContent = textContent;
  }

  return element;
}
