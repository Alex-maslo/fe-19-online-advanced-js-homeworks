"use strict";
export class Form {
  constructor(formId, formTitle) {
    this.formId = formId;
    this.formTitle = formTitle;
  }

  render() {
    const formElement = document.createElement("form");
    const titleForm = document.createElement("h3");

    titleForm.innerText = this.formTitle;

    formElement.setAttribute("id", this.formId);
    formElement.append(titleForm);

    return formElement;
  }

  createSelect(options, classNames, name) {
    const selectElement = document.createElement("select");
    selectElement.classList.add(classNames);
    selectElement.name = name;

    options.forEach((optionText) => {
      const option = document.createElement("option");
      option.textContent = optionText;
      selectElement.appendChild(option);
    });

    return selectElement;
  }

  createLabel(text, className) {
    const labelElement = document.createElement("label");
    labelElement.classList.add(className);
    labelElement.textContent = text;
    return labelElement;
  }

  createInput(type, name, className, id) {
    const inputElement = document.createElement("input");
    inputElement.classList.add(className);
    inputElement.type = type;
    inputElement.name = name;
    inputElement.id = id;
    inputElement.required = true;
    return inputElement;
  }

  createTextarea(name, className) {
    const textareaElement = document.createElement("textarea");
    textareaElement.classList.add(className);
    textareaElement.name = name;
    return textareaElement;
  }

  createButton(type, text, className) {
    const buttonElement = document.createElement("button");
    buttonElement.classList.add(className);
    buttonElement.type = type;
    buttonElement.textContent = text;
    return buttonElement;
  }

  createParagraph(text) {
    const paragraphElement = document.createElement("p");
    paragraphElement.className = "message";
    paragraphElement.innerText = text;
    return paragraphElement;
  }
}
