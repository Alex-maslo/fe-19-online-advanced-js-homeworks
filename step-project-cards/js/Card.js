"use strict";

import { Data } from "./Data.js";

export class Card {
  constructor() {}
  render(object) {
    const {
      doctor,
      purposeVisit,
      visitDescription,
      priority,
      patientName,
      age,
      id,
      lastvisit,
      pressure,
      bodyMassIndex,
      cardiovascular,
    } = object;

    const cardBody = document.createElement("div");
    const cardContent = document.createElement("div");
    const cardCloseButton = document.createElement("div");

    cardBody.classList.add("card__body");
    cardContent.classList.add("card__content");
    cardCloseButton.classList.add("card__close-button");
    cardBody.setAttribute("data-id", id);
    cardCloseButton.onclick = (event) => this.deletCard(event);
    cardCloseButton.innerHTML = "&times;";
    cardContent.innerHTML = `
    <h3 class="card__title">${patientName}</h3>
      <p class="card__doctor">Лікар: ${doctor}</p>
      <button class="card__button card__button--edit">Редагувати</button>
      <button class="card__button card__button--show-more">Показати більше</button>`;

    const hideInfo = document.createElement("div");
    hideInfo.classList.add("card__content");
    hideInfo.classList.add("hide-info");

    // console.log(object);
    for (const prop in object) {
      if (
        object.hasOwnProperty(prop) &&
        prop !== "patientName" &&
        prop !== "doctor" &&
        prop !== "id"
      ) {
        const hideText = document.createElement("p");
        hideText.classList.add("card__content-hide-text");

        hideText.innerHTML = `${this.getLabelForProperty(prop)} : ${
          object[prop]
        }`;
        hideInfo.append(hideText);
      }
    }

    cardContent.append(hideInfo);
    cardBody.append(cardCloseButton, cardContent);
    document.querySelector(".card__container").append(cardBody);
  }

  deletCard(event) {
    const cardBody = event.target.closest(".card__body");
    if (cardBody) {
      const dataId = cardBody.dataset.id;
      const data = new Data();
      data.deleteData(dataId).then(() => {
        data.getData().then((array) => {
          if (array.length == 0) {
            const messageNoItems = document.createElement("p");
            messageNoItems.classList.add("body__alert-text");
            messageNoItems.textContent = "No card items";
            document.querySelector(".card__container").append(messageNoItems);
          }
        });
      });
      cardBody.remove();
    }
  }

  getLabelForProperty(prop) {
    switch (prop) {
      case "purposeVisit":
        return "Мета візиту";
      case "visitDescription":
        return "Опис візиту";
      case "priority":
        return "Терміновість";
      case "age":
        return "Вік";
      case "lastvisit":
        return "Дата останнього візиту";
      case "pressure":
        return "Тиск";
      case "bodyMassIndex":
        return "Індекс маси тіла";
      case "cardiovascular":
        return "Кардіозахворювання";

      default:
        return prop;
    }
  }
}
