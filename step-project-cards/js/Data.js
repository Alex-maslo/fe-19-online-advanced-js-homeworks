"use strict";
import { Card } from "./Card.js";

export class Data {
  constructor(modal) {
    this.modal = modal;
    this.token = localStorage.getItem("token");
  }

  authorization() {
    fetch("https://ajax.test-danit.com/api/v2/cards/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: document.querySelector("#login-input").value,
        password: document.querySelector("#password-input").value,
      }),
    })
      .then((response) => {
        if (response.ok) {
          return response.text();
        } else {
          throw new Error(response.status);
        }
      })

      .then((token) => {
        document.querySelector(".modal__content .message").style.opacity = "0";
        localStorage.setItem("token", token);
        this.authorizationSucsses();
        this.getData().then((array) => {
          console.log(array);

          if (array.length == 0) {
            const messageNoItems = document.createElement("p");
            messageNoItems.classList.add("body__alert-text");
            messageNoItems.textContent = "No items have been added";
            document.querySelector(".card__container").append(messageNoItems);
          }

          array.forEach((obj) => {
            const card = new Card();
            card.render(obj);
          });
        });
      })
      .catch((error) => {
        document.querySelector(".modal__content .message").style.opacity = "1";
        console.error(error);
      });
  }

  getData() {
    return fetch("https://ajax.test-danit.com/api/v2/cards", {
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Ошибка получения данных");
        }
      })
      .catch((error) => {
        console.error(error);
      });
  }

  authorizationSucsses() {
    const headerButton = document.querySelector(".header__button");
    headerButton.textContent = "Створити візит";
    headerButton.setAttribute("id", "visit");
    this.modal.close();
  }

  postData(object) {
    return fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.token}`,
      },
      body: JSON.stringify(object),
    })
      .then((response) => response.json())
      .then((response) => {
        const card = new Card();
        card.render(response);
      });
  }

  editData(id, newData) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${this.token}`,
      },
      body: JSON.stringify(newData),
    })
      .then((response) => response.json())
      .then((response) => {
        return response;
        const card = new Card();
        card.render(response);
      });
  }

  deleteData(id) {
    return fetch(`https://ajax.test-danit.com/api/v2/cards/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${this.token}`,
      },
    })
      .then((response) => {
        if (response.ok) {
          return true;
        } else {
          throw new Error("Ошибка удаления данных");
        }
      })
      .catch((error) => {
        console.error(error);
        return false;
      });
  }
}
