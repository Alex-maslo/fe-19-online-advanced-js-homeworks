"use strict";
import { Form } from "./Form.js";

export class VisitForm extends Form {
  constructor(formId, formTitle) {
    super(formId, formTitle);
  }

  render() {
    const formElement = super.render();

    const selectElement = this.createSelect(
      ["--Виберіть лікаря--", "Кардіолог", "Стоматолог", "Терапевт"],
      "visit-form__select",
      "doctor"
    );

    selectElement.addEventListener("change", (event) => {
      switch (event.target.value) {
        case "Кардіолог":
          document
            .querySelectorAll(".cardio, .dantist, .therapist")
            .forEach((e) => e.remove());

          this.addCardiologistFields().forEach((element) => {
            formElement.insertBefore(element, submitButton);
          });
          break;

        case "Стоматолог":
          document
            .querySelectorAll(".cardio, .dantist, .therapist")
            .forEach((e) => e.remove());

          this.addDentistFields().forEach((element) => {
            formElement.insertBefore(element, submitButton);
          });
          break;

        case "Терапевт":
          document
            .querySelectorAll(".cardio, .dantist, .therapist")
            .forEach((e) => e.remove());

          this.addTherapistFields().forEach((element) => {
            formElement.insertBefore(element, submitButton);
          });
          break;

        default:
          document
            .querySelectorAll(".cardio, .dantist, .therapist")
            .forEach((e) => e.remove());
          break;
      }
    });

    const labelMeta = this.createLabel("Мета візиту");
    const inputMeta = this.createInput("text", "purposeVisit");

    const labelDescription = this.createLabel("Короткий опис візиту");
    const textareaDescription = this.createTextarea(
      "visitDescription",
      "visit-form__textarea"
    );

    const prioritySelect = this.createSelect(
      ["звичайна", "пріоритетна", "невідкладна"],
      "visit-form__select",
      "priority"
    );

    const labelName = this.createLabel("Прізвище, Ім'я, По-батькові");
    const inputName = this.createInput("text", "patientName");

    const submitButton = this.createButton(
      "submit",
      "Відправити",
      "visit-form__button"
    );

    formElement.append(
      selectElement,
      labelMeta,
      inputMeta,
      labelDescription,
      textareaDescription,
      prioritySelect,
      labelName,
      inputName,
      submitButton
    );

    return formElement;
  }

  addCardiologistFields() {
    const normalPressureLabel = this.createLabel("Звичайний тиск", "cardio");
    const normalPressureInput = this.createInput("text", "pressure", "cardio");

    const bodyMassIndexLabel = this.createLabel("Індекс маси тіла", "cardio");
    const bodyMassIndexlInput = this.createInput(
      "text",
      "bodyMassIndex",
      "cardio"
    );

    const cardiovascularLabel = this.createLabel(
      "Перенесені захворювання серцево-судинної системи",
      "cardio"
    );
    const cardiovascularInput = this.createInput(
      "text",
      "cardiovascular",
      "cardio"
    );

    const ageLabel = this.createLabel("Вік", "cardio");
    const ageInput = this.createInput("text", "age", "cardio");

    return [
      normalPressureLabel,
      normalPressureInput,
      bodyMassIndexLabel,
      bodyMassIndexlInput,
      cardiovascularLabel,
      cardiovascularInput,
      ageLabel,
      ageInput,
    ];
  }

  addDentistFields() {
    const lastDateVisitLabel = this.createLabel(
      "Дата останнього відвідування",
      "dantist"
    );
    const lastDateVisitInput = this.createInput("date", "lastvisit", "dantist");

    return [lastDateVisitLabel, lastDateVisitInput];
  }

  addTherapistFields() {
    const ageLabel = this.createLabel("Вік", "therapist");
    const ageInput = this.createInput("text", "age", "therapist");
    return [ageLabel, ageInput];
  }
}
