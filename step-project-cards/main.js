"use strict";

import { Card } from "./js/Card.js";
import { Data } from "./js/Data.js";
import { LoginForm } from "./js/LoginForm.js";
import { Modal } from "./js/Modal.js";
import { VisitForm } from "./js/VisitForm.js";

const headerButton = document.querySelector(".header__button");

const loginFormContent = new LoginForm("login-form", "Авторизація");
const visitFormContent = new VisitForm("visit-form", "Створити візит");

const modalLogin = new Modal(loginFormContent.render());
const modalVisit = new Modal(visitFormContent.render());

const data = new Data(modalLogin);
const cardContainer = document.querySelector(".card__container");

// localStorage.clear();

document.addEventListener("DOMContentLoaded", async () => {
  if (localStorage.getItem("token")) {
    headerButton.textContent = "Створити візит";
    headerButton.setAttribute("id", "visit");
    const arrayCardInfo = await data.getData();
    arrayCardInfo.forEach((obj) => {
      const card = new Card();
      card.render(obj);
    });

    if (arrayCardInfo.length == 0) {
      const messageNoItems = document.createElement("p");
      messageNoItems.classList.add("body__alert-text");
      messageNoItems.textContent = "No items have been added";
      cardContainer.append(messageNoItems);
    }
  }
});

document.addEventListener("click", (event) => {
  //
  if (event.target.classList.contains("card__button--show-more")) {
    const cardBody = event.target.closest(".card__body");
    const dataId = cardBody.dataset.id;

    const hideInfo = document.querySelector(`[data-id="${dataId}"] .hide-info`);
    hideInfo.classList.toggle("show-info");
  }

  if (event.target.classList.contains("card__button--edit")) {
    const editFormContent = new VisitForm("edit-form", "Редагувати візит");
    const modalEdit = new Modal(editFormContent.render());
    modalEdit.render();

    const cardBody = event.target.closest(".card__body");
    const cardId = cardBody.dataset.id;

    const newForm = document.querySelector("#edit-form");

    newForm.addEventListener("submit", (event) => {
      event.preventDefault();

      const editData = new FormData(newForm);
      const editDataValues = Object.fromEntries(editData.entries());

      modalEdit.close();

      data.editData(cardId, editDataValues).then(async () => {
        const arrayCardInfo = await data.getData();

        document.querySelectorAll(".card__body").forEach((el) => el.remove());
        arrayCardInfo.forEach((obj) => {
          const card = new Card();
          card.render(obj);
        });
      });
    });
  }

  switch (event.target.id) {
    case "login":
      modalLogin.render();
      break;

    case "visit":
      modalVisit.render();
      break;

    default:
      break;
  }
});

document.addEventListener("submit", (event) => {
  if (event.target.tagName === "FORM") {
    event.preventDefault();

    switch (event.target.id) {
      case "login-form":
        data.authorization();
        break;

      case "visit-form":
        if (document.querySelector(".body__alert-text")) {
          document.querySelector(".body__alert-text").remove();
        }

        const formData = new FormData(event.target);
        const formObject = {};
        formData.forEach((value, key) => {
          formObject[key] = value;
        });
        data.postData(formObject);
        modalVisit.close();
        break;

      default:
        break;
    }
  }
});

// filter

const filterCard = document.querySelector(".visit-filter");

filterCard.addEventListener("input", async (event) => {
  const inputValue = event.target.value.toLowerCase();
  const arrayCardInfo = await data.getData();
  const allCard = document.querySelectorAll(".card__body");
  allCard.forEach((e) => e.remove());

  arrayCardInfo
    .filter((obj) => {
      const valuesToCheck = Object.values(obj).map((value) =>
        value.toString().toLowerCase()
      );
      return valuesToCheck.some((value) => value.includes(inputValue));
    })
    .forEach((obj) => {
      const card = new Card();
      card.render(obj);
    });
});
