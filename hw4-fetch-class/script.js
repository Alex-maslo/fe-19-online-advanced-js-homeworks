"use strict";

const urlApi = "https://ajax.test-danit.com/api/swapi/films";

class Films {
  constructor(url) {
    this.url = url;
  }

  getData(url) {
    return fetch(url)
      .then((response) => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Ошибка сети");
        }
      })
      .then((data) => {
        return data;
      })
      .catch((error) => {
        console.error(error);
      });
  }

  getFilms(data) {
    const array = data.map(({ episodeId, openingCrawl, name }) => {
      const filmName = document.createElement("h3");
      filmName.innerHTML = `Епізод ${episodeId}: ${name}`;
      const filmOpeningCrawl = document.createElement("p");
      filmOpeningCrawl.innerHTML = `<span style='font-weight: 700'>Короткий опис епізоду:</span>${openingCrawl}`;
      return [filmName, filmOpeningCrawl];
    });

    return array;
  }

  getCharacters(data) {
    return data.map(({ characters }) => {
      const charList = document.createElement("ul");
      const arrRequest = characters.map((charUrls) => {
        this.getData(charUrls).then((char) => {
          const charListElement = document.createElement("li");
          charList.append(charListElement);
          charListElement.textContent = char.name;
          return charListElement;
        });
        return charList;
      });
      console.log(arrRequest);
      return arrRequest;
    });
  }

  renderFilmList() {
    const filmList = document.createElement("ul");
    filmList.style.listStyle = "none";
    document.querySelector("#root").append(filmList);

    this.getData(this.url).then((data) => {
      for (let i = 0; i < data.length; i++) {
        const filmListElement = document.createElement("li");
        filmListElement.append(...this.getFilms(data)[i]);
        filmListElement.append(...this.getCharacters(data)[i]);
        filmList.append(filmListElement);
      }
    });
  }
}

const films = new Films(urlApi);
films.renderFilmList();
